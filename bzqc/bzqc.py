#!/usr/bin/env fslpython
"""
bzqc is a tool to help users configure eddy and topup related inputs.
This program will create eddy inputs from dicom data, or from the .json files of files already converted to nifti with BIDS info.
"""

import argparse
from os.path import splitext, join, basename, abspath, exists, dirname
from os import makedirs
from subprocess import run
from glob import glob
import sys
import matplotlib.pyplot as plt
import nibabel as nii
import numpy as np
from numpy import zeros, round, eye, transpose, squeeze, concatenate, array, append, vstack, dot, \
    log, argsort
from scipy.ndimage import center_of_mass
from scipy.linalg import pinv, det
from numpy.linalg import matrix_rank as rank
from scipy.fftpack import dct
from scipy.stats import chi2
import json
import logging

logger = logging.getLogger(__name__)
formatter = logging.Formatter(
    fmt='%(asctime)s - %(message)s',
    datefmt='%d-%b-%y %H:%M:%S'
)
ch = logging.StreamHandler()
ch.setFormatter(formatter)
logger.addHandler(ch)


# setup command line options
parser = argparse.ArgumentParser(
    description="bzqc helps you get all the eddy inputs from dicom files or from BIDS json data"
)
parser.add_argument(
    'dicom_dir',
    help='the directory with your dicom files to be converted to nifti + BIDS json',
    type=str
)
parser.add_argument(
    '--input',
    help="The input image to test for b0 motion artefact (overrides dicom_dir as input)",
    type=str
)
parser.add_argument(
    '--json',
    help="The BIDS JSON file describing image meta data (we need this for slice timing).",
    type=str
)
parser.add_argument(
    '--bvals',
    help="The text file containing the bvals for each volume",
    type=str
)
parser.add_argument(
    '--nd',
    help="The number of drift components in the model",
    default=12,
    type=int
)
parser.add_argument(
    '--nm',
    help="The number of motion components in the model",
    default=6,
    type=int
)
parser.add_argument(
    '--save',
    help="Save an image of the data plots",
    action='store_true',
)
parser.add_argument(
    '--v',
    help="verbose mode. Will output more text, show intermediate plots, and save results figures for each volume",
    default=False,
    action='store_true',
)


def nvols(inimg):
    """
    compute number of volumes for input image
    """
    s = inimg.shape
    vols = None
    if len(s) < 3:
        ValueError('input image must be a 3D or 4D image')
    if len(s) == 3:
        # if only 3D then there is only one volume
        vols = 1
    if len(s) == 4:
        # if 4D then nvols is the size of the 4th dimension
        vols = s[-1]  # indexed from 0
    return vols


def nslices(inimg):
    """
    compute number of slices
    """
    s = inimg.shape
    if len(s) < 3:
        ValueError('input image must be a 3D or 4D image')
    return s[2]


def com(inimg):
    """
    get center of mass coordinates for each axial slice in inimg
    """
    s = inimg.shape
    X = array([])
    Y = array([])
    # if 2D
    if len(s) == 2:
        x, y = center_of_mass(inimg)
        X = append(X, x)
        Y = append(Y, y)
        return X, Y
    # if 3D
    elif len(s) == 3:
        for idx in range(nslices(inimg)):
            x, y, = center_of_mass(inimg[:, :, idx])
            X = append(X, x)
            Y = append(Y, y)
        return X, Y


def get_slice_time(f, ns):
    """
    get slice time info
    """
    if f is None:
        t = zeros((ns, 1))
        for i in range(1, ns + 1, 2):
            t[i - 1] = (i - 1) * 0.05

        for i in range(2, ns + 1, 2):
            t[i - 1] = (ns + i - 1) * 0.05
        return t

    return read_bids(f)


def read_bids(f):
    """
    read bids formatted json file and get slice timing, MB factor, and whether or not the image is diffusion
    """
    with open(f, 'r') as bds:
        data = json.load(bds)
    isdiff = False
    st = None
    mb = 1
    if "DIFFUSION" in data['ImageType']:
        isdiff = True
    if 'SliceTiming' in data.keys():
        st = squeeze(array(data['SliceTiming']))
    if 'MultibandAccelerationFactor' in data.keys():
        mb = data['MultibandAccelerationFactor']

    return st, mb, isdiff


def read_bvals(f):
    """
    read b values from text file generated from dcm2niix
    """
    lines = [line.rstrip('\n').strip() for line in open(f)]
    b = []
    for line in lines:
        split = line.split()
        for val in split:
            b.append(int(val))
    return array(b)


def dct_full(s, n, o):
    """
    compute DCT of full model, ordered by slice acquisition
    s: number of slices
    n: number of components to keep from model
    o: slice order indices
    """
    # DCT basis series expansion ordered by slice acquisition
    D = dct(eye(s), norm='ortho', axis=0)
    model = transpose(squeeze(D[1:n + 1, o]))
    # print(model.shape)
    if SHOW_PLOTS:
        plt.plot(model)
        plt.show()
    return model


def dct_drift(s, n):
    """
    compute DCT of full model, ordered by slice acquisition
    s: number of slices
    n: number of components to keep from model
    """
    # DCT basis series modeling low frequency center of mass drift
    DD = dct(eye(s), norm='ortho', axis=0)
    model = transpose(squeeze(DD[0:n, :]))
    # print(model.shape)
    if SHOW_PLOTS:
        plt.plot(model)
        plt.show()
    return model


def beta(model, y):
    """
    compute betas
    :param model: design of our model
    :param y: our observed data
    """
    return dot(pinv(model), y)


def sigma(model, y, b, n):
    """

    :param model: our design model
    :param y: our observed data
    :param b: our computed betas
    :param n: the number of slices in this volume
    """
    diffs = y - dot(model, b)
    a = 1 / n
    x = dot(diffs.transpose(), diffs)
    s = x * a
    return s


def wilks(s1, s2):
    """
    compute wilks lambda
    :param s1:
    :param s2:
    """
    return det(s1) / det(s2)


def chi_p(model1, model2, y, lm, n):
    """
    compute chi-square approximation to turn this into an actual statistical test
    :param model1:
    :param model2:
    :param y:
    :param lm:
    :param n:
    """
    rd = rank(model1)
    rdr = rank(model2)
    w = y.shape[1]
    chi_approx = - (n - rd - 1 - 0.5 * (w - rd + rdr + 1)) * log(lm)
    df = w * (rd - rdr)
    logger.debug("df is: {}".format(df))
    p_val = 1 - chi2.cdf(chi_approx, df)
    logger.debug("pval is: {}".format(p_val))
    return p_val

def make_plot(I, f, fig, axes, input_file, v, predicted_full, predicted_part, coms):
    """
    generate a QC plot
    """
    fig.suptitle("file: {}\nvol: {} (zero indexed)".format(basename(input_file), v), fontsize=14)
    axes[0].plot(predicted_full)
    axes[0].plot(coms)
    axes[0].set_xlabel('Slices')
    axes[0].set_ylabel('Center of mass')
    axes[0].set_title('Full model with high frequency motion')
    axes[0].legend(["Estimated com x", "Estimated com y", "com x", "com y"], loc='lower right')

    axes[1].imshow(I[int(f), :, :].T, cmap=plt.cm.gray, interpolation='nearest', origin="lower", animated=True)

    axes[2].plot(predicted_part)
    axes[2].plot(coms)
    axes[2].set_xlabel('Slices')
    axes[2].set_ylabel('Center of mass')
    axes[2].set_title('Reduced model for low freq mass drift')
    axes[2].legend(["Estimated com x", "Estimated com y", "com x", "com y"], loc='lower right')


def convert_dicoms(dicom_dir):
    """
    use dcm2niix to convert from dicom to nifti file format, also generate json sidecar files for meta data
    """
    os = sys.platform
    d2nver = None
    if os == 'darwin':
        d2nver = 'mac'
    elif os == 'linux':
        d2nver = 'lnx'
    if d2nver is None:
        logger.error("error: operating system is not detected as linux or macOS. sys.platform returned {}".format(os))
        sys.exit()

    exe = join(dirname(abspath(__file__)), "dcm2niix_{}".format(d2nver), 'dcm2niix')
    out_dir = join(abspath(dicom_dir), "converted")
    if not exists(out_dir):
        makedirs(out_dir)
        logger.info("created the output directory at {}".format(out_dir))

    cmd = [
        exe,
        "-b",
        "y",
        "-d",
        str(0),
        "-o",
        out_dir,
        "-z",
        "i",
        abspath(dicom_dir)
    ]
    logger.debug("dcm2niix command: {}".format(" ".join(cmd)))
    output = run(cmd, capture_output=True)

    if output.returncode != 0:
        logger.error("error: dcm2niix reported this error: {}".format(output.stderr.decode("utf-8")))
        sys.exit()

    niigz_files = glob(join(out_dir, "*.nii.gz"))
    json_files = glob(join(out_dir, "*.json"))
    bval_files = glob(join(out_dir, "*.bval"))

    return json_files, niigz_files, bval_files


SHOW_PLOTS = False


def main():
    """
    the main entry point for the bzqc program
    """
    # parse arguments
    args = parser.parse_args()
    dicom_dir = args.dicom_dir
    input_file = args.input
    json_file = args.json
    bvals_file = args.bvals
    nd = args.nd
    nm = args.nm
    save_fig = args.save
    verbose_mode = args.v
    # TODO change SHOW_PLOTS to verbose_mode
    if verbose_mode:
        LOGLEVEL = logging.DEBUG
    else:
        LOGLEVEL = logging.WARN

    logger.setLevel(LOGLEVEL)

    use_dicom = False
    use_input_file = False
    # check some args
    # if no overriding input file given, assume the user wants to convert a dicom dir. In this case, it should exist
    if input_file is None:
        if not exists(abspath(dicom_dir)):
            logger.error('error: cannot access {} or it does not exist'.format(dicom_dir))
            sys.exit()
        use_dicom = True
    else:
        logger.warning('warning: using supplied input file rather than converting dicom images')
        if not exists(abspath(input_file)):
            logger.error('error: cannot access {} or it does not exist'.format(input_file))
            sys.exit()
        use_input_file = True

    json_files = []
    diff_files = []
    bval_files = []
    if use_dicom:
        # convert dicom images to nifti + json sidecar metadata
        json_files, diff_files, bval_files = convert_dicoms(dicom_dir)
        logger.debug(json_files)
        logger.debug(diff_files)
        logger.debug(bval_files)

    if len(json_files) == 0:
        logger.error("error: no json files found. something has gone wrong")
        sys.exit()
    if len(diff_files) == 0:
        logger.error("error: no nifti files found. something has gone wrong")
        sys.exit()

    if len(diff_files) != len(json_files):
        logger.error("error: number of json files and nifti files does not match. something has gone wrong")
        sys.exit()
    # n sets is the number of datasets found after dicom conversion (a dataset is a json + nii.gz pair)
    n_sets = len(json_files)

    for i in range(n_sets):
        json_file = json_files[i]
        # get json file value from json input option
        isdiff = False
        st = array([])
        mb = 1  # assume multiband of 1 (no parallel) as default, will be updated if found in json
        st, mb, isdiff = read_bids(json_files[i])
        logger.debug("is diffusion: {}".format(isdiff))
        if not isdiff:
            logger.warning("warning: diffusion not listed in imagetype field, skipping {}".format(json_file))
            continue

        img_pth = ""
        for imgfile in diff_files:
            if splitext(json_file)[0] == splitext(splitext(imgfile)[0])[0]:
                img_pth = imgfile

        img = nii.load(img_pth)
        img_data = img.get_fdata()

        if len(st) == 0:
            logger.error("error: no slice timing info in slice timing array!")
            logger.error("here is the offending array: {}".format(st))
            logger.error("from file: {}".format(json_files[i]))
            sys.exit()

        bvals = np.array([])
        bval_pth = ""
        if len(bval_files) > 0:
            for bfile in bval_files:
                if splitext(bfile)[0] == splitext(splitext(img_pth)[0])[0]:
                    bval_pth = bfile
            # compare bval basename to nii.gz basename
            if splitext(bval_pth)[0] == splitext(splitext(img_pth)[0])[0]:
                bvals = read_bvals(bval_pth)
        else:
            logger.warning("warning: no bval files! I will check every volume. Might take longer.")

        # if bvals is empty assume that we need to operate on all volumes in the file
        if len(bvals) == 0:
            bidx = range(nvols(img_data))
        else:
            # some low bval can be considered bzero images
            bidx = squeeze(np.argwhere(bvals <= 10))
            logger.debug("bzero images found at indices: {}".format(bidx))

        for v in bidx:
            isbad = False
            # if more than one volume, then get the requested image at index v
            # v = 0
            if len(img_data.shape) > 3:
                I = img_data[:, :, :, v]
            else:
                I = img_data

            # get number of axial slices
            ns = nslices(I)
            logger.debug("number of slices: {}".format(ns))

            # get x and y center of mass location by slice
            com_x, com_y = com(I)
            coms = transpose(vstack((com_x, com_y)))
            if SHOW_PLOTS:
                plt.plot(coms)
                plt.show()

            # convert slice times to slice indices
            n_grps = round(ns/mb)
            grp_starts = squeeze(np.argwhere(st == 0))
            logger.debug(grp_starts)
            gidx = argsort(argsort(st[grp_starts[0]:grp_starts[1]]))
            logger.debug(gidx)
            stindx = np.tile(gidx, mb)
            logger.debug(stindx)

            if SHOW_PLOTS:
                plt.plot(stindx)
                plt.show()
            all_p = []

            model_drift = dct_drift(ns, nd)
            model_sliceordered_dct = dct_full(ns, nm, stindx)
            # concatenate design columns
            design = concatenate((model_drift, model_sliceordered_dct), axis=1)
            # compute betas
            beta_full = beta(design, coms)
            beta_drift = beta(model_drift, coms)
            sigma_full = sigma(design, coms, beta_full, ns)
            sigma_drift = sigma(model_drift, coms, beta_drift, ns)
            # compute Wilk's Lambda between full and partial (drift) models
            # small value (closer to zero than to one) means null should be rejected.
            lam = wilks(sigma_full, sigma_drift)
            logger.debug("lamda is: {}".format(lam))

            p = chi_p(design, model_drift, coms, lam, ns)
            all_p.append(p)
            p_thresh = 0.25
            predicted_full = dot(design, beta_full)
            predicted_part = dot(model_drift, beta_drift)

            if save_fig:
                # setup figure
                fig, axes = plt.subplots(1, 3, figsize=(15, 6))
                # fill in the figure
                make_plot(I, int(round(img_data.shape[0] / 2) + 6), fig, axes, img_pth, v, predicted_full, predicted_part, coms)
                plt.savefig(splitext(splitext(img_pth)[0])[0] + "_vol_{0:03d}".format(v) + '.png', dpi=120)
                plt.close()
            else:
                pass

            # write results to file
            results_p = splitext(splitext(img_pth)[0])[0] + "_vol_{0:03d}".format(v) + '.txt'
            with open(results_p, 'w') as resultsfile:
                for j, pp in enumerate(all_p):
                    resultsfile.write("p = {}\n".format(all_p[j]))

            if p <= p_thresh:
                isbad = True
            if isbad:
                logger.info(splitext(splitext(img_pth)[0])[0] + " volume {}".format(v) + " is bad")


if __name__ == '__main__':
    sys.exit(main())
